INSERT INTO user (id, first_name, last_name) VALUES
  (1000, 'lala', 'lala'),
  (1001, 'lolo', 'lolo');

INSERT INTO customer (id, name, enabled, expiry_date) VALUES
  (1000, 'audi', true, NULL),
  (1001, 'ford', true, NULL);

INSERT INTO product (id, name, type, customer_id) VALUES
  (1000, 'pippo', 'type1', 1000),
  (1001, 'pluto', 'type2', 1000);