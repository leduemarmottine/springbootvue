package de.jonashackt.springbootvuejs;

import de.jonashackt.springbootvuejs.domain.Customer;
import de.jonashackt.springbootvuejs.domain.Product;
import de.jonashackt.springbootvuejs.domain.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class SpringBootVuejsApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(SpringBootVuejsApplication.class, args);
	}
	
	@Profile("dev")
	@Component
	public static class SpringDataRestCustomization extends RepositoryRestConfigurerAdapter {

	  @Override
	  public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {

	    config.getCorsRegistry().addMapping("/**")
	      .allowedOrigins("http://localhost:8080");
	      //.allowedMethods("PUT", "DELETE")
	      //.allowedHeaders("header1", "header2", "header3")
	      //.exposedHeaders("header1", "header2")
	      //.allowCredentials(false).maxAge(3600);
	  }
	}

	@Component
	public class ExposeEntityIdRestMvcConfiguration extends RepositoryRestConfigurerAdapter {

		@Override
		public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
			config.exposeIdsFor(Customer.class);
			config.exposeIdsFor(Product.class);
			config.exposeIdsFor(User.class);
		}
	}
}
