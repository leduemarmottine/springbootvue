package de.jonashackt.springbootvuejs.repository;


import de.jonashackt.springbootvuejs.domain.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findByName(@Param("name") String name);

    List<Product> findByType(@Param("type") String type);

}
