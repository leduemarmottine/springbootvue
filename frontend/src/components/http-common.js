import axios from 'axios'

export const AXIOS = axios.create({
  baseURL: `/api`
})

export function encodeQueryData(paramsObject) {
  let result = [];
  for (let param in paramsObject) {
    result.push(encodeURIComponent(param) + '=' + encodeURIComponent(paramsObject[param]));
  }
  return result.join('&');
}
