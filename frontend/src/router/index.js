import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Service from '@/components/Service'
import Bootstrap from '@/components/Bootstrap'
import User from '@/components/User'
import ProductForm from '@/components/ProductForm'
import CustomerList from '@/components/CustomerList'
import CustomerForm from '@/components/CustomerForm'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/callservice',
      name: 'Service',
      component: Service
    },
    {
      path: '/bootstrap',
      name: 'Bootstrap',
      component: Bootstrap
    },
    {
      path: '/user',
      name: 'User',
      component: User
    },
    {
      path: '/productForm',
      name: 'ProductForm',
      component: ProductForm
    },
    {
      path: '/customerList',
      name: 'CustomerList',
      component: CustomerList
    },
    {
      path: '/customer/:id',
      name: 'CustomerForm',
      component: CustomerForm,
      props: true
    }
  ]
})
